package com.esentri.adf.samples.cascadinglovs.beans;

import java.io.Serializable;

import oracle.adf.controller.TaskFlowId;

public class RegionHandler implements Serializable {
    private String taskFlowId = "/WEB-INF/taskflows/searchFormTF.xml#searchFormTF";

    public RegionHandler() {
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public String searchForm() {
        setDynamicTaskFlowId("/WEB-INF/taskflows/searchFormTF.xml#searchFormTF");
        return null;
    }

    public String editFormTF() {
        setDynamicTaskFlowId("/WEB-INF/taskflows/editFormTF.xml#editFormTF");
        return null;
    }
}
