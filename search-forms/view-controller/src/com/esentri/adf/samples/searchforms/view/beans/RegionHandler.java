package com.esentri.adf.samples.searchforms.view.beans;

import java.io.Serializable;

import oracle.adf.controller.TaskFlowId;

public class RegionHandler implements Serializable {
    private String taskFlowId = "/WEB-INF/taskflows/customFormTF.xml#customFormTF";

    public RegionHandler() {
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public String customFormTF() {
        setDynamicTaskFlowId("/WEB-INF/taskflows/customFormTF.xml#customFormTF");
        return null;
    }

    public String queryPanelTF() {
        setDynamicTaskFlowId("/WEB-INF/taskflows/queryPanelTF.xml#queryPanelTF");
        return null;
    }

    public String otherFormFeaturesTF() {
        setDynamicTaskFlowId("/WEB-INF/taskflows/otherFormFeaturesTF.xml#otherFormFeaturesTF");
        return null;
    }

    public String customFormWithTransientVoTF() {
        setDynamicTaskFlowId("/WEB-INF/taskflows/customTransientFormTF.xml#customTransientFormTF");
        return null;
    }
}
