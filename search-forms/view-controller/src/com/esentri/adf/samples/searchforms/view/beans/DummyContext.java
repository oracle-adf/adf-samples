package com.esentri.adf.samples.searchforms.view.beans;

public class DummyContext {

    private String defaultLocationId;

    public void setDefaultLocationId(String defaultLocationId) {
        this.defaultLocationId = defaultLocationId;
    }

    public String getDefaultLocationId() {
        return defaultLocationId;
    }
}
