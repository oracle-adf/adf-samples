package com.esentri.adf.samples.dynamictabs.beans;

import com.esentri.adf.samples.dynamictabs.TabContext;

import java.io.IOException;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.controller.ControllerContext;

public class UIShell extends DynamicTabsBackingBean {

    public void launchTfOne(ActionEvent actionEvent) {
        _launchActivity("tf-one", "/WEB-INF/taskflows/tf-one.xml#tf-one", true, null);
    }

    public void launchTfTwo(ActionEvent actionEvent) {
        _launchActivity("tf-two", "/WEB-INF/taskflows/tf-two.xml#tf-two", true, null);
    }

    public void launchTfThree(ActionEvent actionEvent) {
        _launchActivity("tf-three", "/WEB-INF/taskflows/tf-three.xml#tf-three", true, null);
    }
    
    public void launchTfEmployees(ActionEvent actionEvent) {
        _launchActivity("employees", "/WEB-INF/taskflows/employees-tf.xml#employees-tf", true, null);
    }
    
    public void refreshPage(ActionEvent evt) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        String page = fctx.getViewRoot().getViewId();
        ViewHandler ViewH = fctx.getApplication().getViewHandler();
        UIViewRoot UIV = ViewH.createView(fctx, page);
        UIV.setViewId(page);
        fctx.setViewRoot(UIV);
    }

    public String getTabsCount() {
        int t = TabContext.getCurrentInstance().getNumberOfTabs();
        return Integer.toString(t);
    }
    
    public void onRedirectLink(ActionEvent evt){
        redirectToSelf();
    }
    
    private void redirectToSelf() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        String viewId = fctx.getViewRoot().getViewId();
        ControllerContext controllerCtx = null;
        controllerCtx = ControllerContext.getInstance();
        String activityURL = controllerCtx.getGlobalViewActivityURL(viewId);
        try {
            ectx.redirect(activityURL);
            fctx.responseComplete();
        } catch (IOException e) {
            //Can't redirect
            e.printStackTrace();
            fctx.renderResponse();
        }
    }
}
