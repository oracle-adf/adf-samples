package com.esentri.adf.samples.dynamictabs.beans;

import java.util.HashMap;
import java.util.Map;

import javax.faces.event.ActionEvent;

public class View2 extends DynamicTabsBackingBean{

    private String inputParameter;

    public void launchTfThreeWithParams(ActionEvent actionEvent) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("inputParameter", inputParameter);
        _launchActivity("tf-three", "/WEB-INF/taskflows/tf-three.xml#tf-three", true, params);
    }

    public void setInputParameter(String inputParameter) {
        this.inputParameter = inputParameter;
    }

    public String getInputParameter() {
        return inputParameter;
    }
}
