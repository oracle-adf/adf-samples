package com.esentri.adf.samples.dynamictabs.beans;

import com.esentri.adf.samples.dynamictabs.TabContext;

import java.util.Map;

public class DynamicTabsBackingBean {
    public void _launchActivity(String title, String taskflowId, boolean newTab, Map<String, Object> parameters) {
        try {
            if (newTab) {
                TabContext.getCurrentInstance().addTab(title, taskflowId, parameters);
            } else {
                TabContext.getCurrentInstance().addOrSelectTab(title, taskflowId, parameters);
            }
        } catch (TabContext.TabOverflowException toe) {
            // causes a dialog to be displayed to the user saying that there are
            // too many tabs open - the new tab will not be opened...
            toe.handleDefault();
        }

    }
}
