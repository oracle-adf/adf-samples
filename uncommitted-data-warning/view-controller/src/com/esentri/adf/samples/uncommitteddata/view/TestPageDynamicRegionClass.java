package com.esentri.adf.samples.uncommitteddata.view;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.application.Application;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCDataControl;
import oracle.jbo.ApplicationModule;
import oracle.jbo.Transaction;

import oracle.adf.controller.TaskFlowId;

public class TestPageDynamicRegionClass implements Serializable {
    private String taskFlowId;

    private List<String> fragmentList = new ArrayList<>();
    private int currentNumber = 0;

    public TestPageDynamicRegionClass() {
        fragmentList.add("/WEB-INF/taskflows/uncommittedChangesTF.xml#uncommittedChangesTF");
        fragmentList.add("/WEB-INF/taskflows/dummyTF.xml#dummyTF");
        updateRegion();
    }
    
    private void updateRegion() {
        taskFlowId = fragmentList.get(currentNumber);
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public String dummyTF() {
        setDynamicTaskFlowId("/WEB-INF/taskflows/dummyTF.xml#dummyTF");
        return null;
    }

    public String uncommittedChangesTF() {
        setDynamicTaskFlowId("/WEB-INF/taskflows/uncommittedChangesTF.xml#uncommittedChangesTF");
        return null;
    }
    
    private boolean uncommittedChanges1() {
        return getCurrentTransaction1().isDirty();
    }
    
    private boolean uncommittedChanges2() {
        return getCurrentTransaction2().isDirty();
    }
    
    private Transaction getCurrentTransaction1(){
        //FacesContext fc = FacesContext.getCurrentInstance();
        //ValueBinding vb = fc.getApplication().createValueBinding("#{data}");
        //BindingContext bc = (BindingContext)vb.getValue(fc);
        BindingContext bc =  BindingContext.getCurrent();
        DCDataControl dc  = bc.findDataControl("AppModuleDataControl");
        ApplicationModule am = (ApplicationModule)dc.getDataProvider();  
        return am.getTransaction();
    }
    
    private Transaction getCurrentTransaction2(){
        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = fc.getELContext();
        ValueExpression valueExp =
        elFactory.createValueExpression(elContext, "#{data.AppModuleDataControl.dataProvider}",
        Object.class);
        ApplicationModule am = (ApplicationModule)valueExp.getValue(elContext); 
        return am.getTransaction();
    }
    
    public void next() {
        if(uncommittedChanges1()) {
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "There are uncommitted changes!", null));
            return;
        }
        if(currentNumber < fragmentList.size() - 1) {
            currentNumber++;
        } else {
            currentNumber = 0;
        }
        updateRegion();
    }
    
    public void previous() {
        if(uncommittedChanges2()) {
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "There are uncommitted changes!", null));
            return;
        }
        if(currentNumber > 0) {
            currentNumber--;
        } else {
            currentNumber = fragmentList.size() - 1;
        }
        updateRegion();
    }
}
