package com.esentri.adf.samples.changeskin;

import java.io.IOException;

import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.controller.ControllerContext;
import oracle.adf.share.ADFContext;

public class MyBackingBean {
    
    public void onSkinSelection(ValueChangeEvent valueChangeEvent) {
        
        String newSkin = (String) valueChangeEvent.getNewValue();
        
        ADFContext adfctx = ADFContext.getCurrent();
        Map sessionScope = adfctx.getSessionScope();
        SkinManager skinManager = (SkinManager)sessionScope.get("skinManager");
        skinManager.setSkin(newSkin);
        redirectToSelf();
    }

    private void redirectToSelf() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        String viewId = fctx.getViewRoot().getViewId();
        ControllerContext controllerCtx = null;
        controllerCtx = ControllerContext.getInstance();
        String activityURL = controllerCtx.getGlobalViewActivityURL(viewId);
        try {
            ectx.redirect(activityURL);
            fctx.responseComplete();
        } catch (IOException e) {
            //Can't redirect
            e.printStackTrace();
            fctx.renderResponse();
        }
    }
}
