package com.esentri.common.samples.regionrefresh.view;

import java.util.ArrayList;
import java.util.List;

import oracle.binding.AttributeContext;
import oracle.binding.RowContext;

public class People {

    List<Person> people;

    public People() {
        /* people = new ArrayList<Person>();

        Person person1 = new Person();
        person1.setName("Frank");
        person1.setAge(32);

        Person person2 = new Person();
        person2.setName("Frankolini");
        person2.setAge(33);

        people.add(person1);
        people.add(person2); */
    }


    public void setPeople(List<Person> people) {
        this.people = people;
    }

    public List<Person> getPeople() {
        return people;
    }

    public boolean setAttributeValue(AttributeContext p0, Object p1) {
        return false;
    }

    public Object createRowData(RowContext p0) {
        return null;
    }

    public Object registerDataProvider(RowContext p0) {
        return null;
    }

    public boolean removeRowData(RowContext p0) {
        return false;
    }

    public void validate() {
    }
}
