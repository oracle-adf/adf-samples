package com.esentri.common.samples.regionrefresh.view;

public class RegionRefreshHelper {
    
    private boolean refreshFlag;

    public void setRefreshFlag(boolean refreshFlag) {
        this.refreshFlag = refreshFlag;
    }

    public boolean isRefreshFlag() {
        return refreshFlag;
    }
}
