package com.esentri.samples.internationalization.view.beans;

import java.io.IOException;

import java.io.Serializable;

import java.util.List;
import java.util.Locale;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.controller.ControllerContext;
import oracle.adf.model.BindingContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class LocaleBean implements Serializable{
    @SuppressWarnings("compatibility:-618077882886632121")
    private static final long serialVersionUID = 1L;

    private static final String DEFAULT_LOCALE = "de";

    private Locale preferredLocale;

    private String language;

    public LocaleBean() {
        System.out.println("Creating locale bean...");

        language = DEFAULT_LOCALE;

        Locale locale = new Locale(DEFAULT_LOCALE);
        setPreferredLocale(locale);

        System.out.println("Default locale set: " + getPreferredLocale().toString());
    }


    /*public void changeLocale(String language) {
        System.out.println("changeLocale: " + language);

        Locale newLocale = new Locale(language);
        FacesContext fctx = FacesContext.getCurrentInstance();
        fctx.getViewRoot().setLocale(newLocale);
        setPreferredLocale(FacesContext.getCurrentInstance().getViewRoot().getLocale());
        refreshPage();
    }

    public void onLanguageChange(ValueChangeEvent evt) {
        String value = (String) evt.getNewValue();
        changeLocale(value);
    }*/

    protected void refreshPage() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        String page = fctx.getViewRoot().getViewId();
        ViewHandler ViewH = fctx.getApplication().getViewHandler();
        UIViewRoot UIV = ViewH.createView(fctx, page);
        UIV.setViewId(page);
        fctx.setViewRoot(UIV);
    }

    protected void redirect() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();

        String viewId = fctx.getViewRoot().getViewId();
        System.out.println("viewId: " + viewId);
        ControllerContext controllerCtx = null;
        controllerCtx = ControllerContext.getInstance();
        String activityURL = controllerCtx.getGlobalViewActivityURL(viewId);
        System.out.println("activityURL: " + activityURL);

        try {
            ectx.redirect(activityURL);
        } catch (IOException e) {
            //Can't redirect
            e.printStackTrace();

        }
    }

    public void setLanguage(String language) {
        this.language = language;
        Locale newLocale = new Locale(language);
        setPreferredLocale(newLocale);
        
        //redirect();
        refreshPage();
    }

   

    public String getLanguage() {
        return language;
    }

    public void setPreferredLocale(Locale preferredLocale) {
        System.out.println("setPreferredLocale: " + preferredLocale);

        this.preferredLocale = preferredLocale;
    }

    public Locale getPreferredLocale() {
        System.out.println("getPreferredLocale: " + this.preferredLocale);

        return preferredLocale;
    }
}
